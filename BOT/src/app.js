const puppeteer = require("puppeteer");
const fs = require("fs");
var mysql = require("mysql");

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    // args: ["--start-fullscreen"],
    args: ["--start-maximized"],
  });
  const page = await browser.newPage();
  await page.setDefaultNavigationTimeout(0);
  await page.setViewport({ width: 1366, height: 768 });
  await page.goto("https://www.instagram.com/", { waitUntil: "networkidle2" });

  // Selector Usuario Input
  await page.type(
    "#loginForm > div > div:nth-child(1) > div > label > input",
    "usuariodemo",
    { delay: 100 }
  );
  // Selector Contraseña Input
  await page.type(
    "#loginForm > div > div:nth-child(2) > div > label > input",
    "contraseñademo",
    { delay: 100 }
  );
  // Esperar 1 segundo
  await page.waitFor(1000);
  // Selector button y acer click
  await page.click("#loginForm > div > div:nth-child(3) > button");

  //   await page.screenshot({ path: Math.random() + ".png" });
  //   console.log(await page.content());
  //   await page.close();
})();
