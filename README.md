# BOT-INSTAGRAM

Advance BOT for instagram

Reglas:
  -Al día hará un seguimiento de hasta 150 usuarios (configurable). Usted elige una lista de usuarios de Instagram cuyo mercado desea apuntar. El bot navega a cada uno de ellos, encuentra a las últimas personas que los han seguido y luego sigue a cada uno de ellos. Luego, después de 5 días (también configurable), dejará de seguir a los usuarios. Simple y eficaz.
  -Cambiar la firma del navegador para evitar ser identificado
  -Instagram está endureciendo sus reglas al no permitir comportamientos promiscuos como seguir y dar me gusta a las fotos de extraños tanto como antes, y imponer bloqueos temporales cuando creen que cruzaste el límite.
  -un máximo de seguimientos / abandonos por día 150 y un máximo de 20 por hora, tal vez incluso comience más bajo.

Solo para perfiles públicos
  1)	Like a fotos: dar like a las fotos de forma aleatoria sin seguir al perfil, guardar en una base de datos para después dar un-like
  2)	Unlike a fotos: en 5 dias dar unlike a las fotos.
  3)	Ver historias: ver las historias de forma aleatoria, por ejemplo: si la persona tiene 10 historias, solo ver 3 (el numero 3 es un numero aleatorio según la cantidad de historias de la persona)
  4)	Seguir al perfil: seguir solo a perfiles públicos, guardarlo en una base de datos y a los 5 días cuando se ejecute el script deje de seguir a esos usuarios.
  5)	Dejar de seguir: en 5 días dejar de seguir a los usuarios que se ha seguido por medio del Bot.
